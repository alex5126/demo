import { Injectable } from '@angular/core';
import { Book } from './models/book';
import { books, auth } from './mocks/mock-book';
import { Observable, of } from 'rxjs';
import { Auth, Login } from './models/auth';


@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor() { }

  getBooksFirst(): Book[]{
    return books;
  }

  getBooks(): Observable<Book[]>{
    return of(books);
  }

  getAuth(data: Login): Observable<Auth>{
    return of(auth);
  }
  
}

//import { Requeriment } from '../models/requeriment';

export interface RequerimentFormat {
    id: any,
    Status: string;
    Fecha: any;
    Type: string;
    Code: string;
    Justification: string;
    Area: string;
    EstimatedAmount: number;
    Description: Requeriment[];
}

export interface Requeriment {
    Name:string;
    Unit: string;
    Quantity: number;
}
export interface Auth{
    success: boolean;
    message: string;
}

export interface Login{
    email: string;
    password: string;
};

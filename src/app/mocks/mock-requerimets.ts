import { RequerimentFormat, Log } from "../models/format";

export const Requeriments: RequerimentFormat[] = [{
    id:'',
    Fecha: new Date().toJSON().slice(0,10),
    Status: 'en proceso',
    Code:'123ASD',
    Justification:'SE REQUIERE EL MANTENIMIENTO CORRECTIVO AL CONMUTADOR TELEFONICO DELEGACIONAL, LO ANTERIOR OBEDECE A MANTENER A ESTE SISTEMA',
    Type:'SERVICIO',
    Area:'Area 1',
    EstimatedAmount: 0.00,
    Description : [
      {Name:'MANTENIMIENTO PREVENTIVO A CONMUTADOR TELEFONICO',Quantity:1,Unit:'SERVICIO'},
      {Name:'MANTENIMIENTO CORRECTIVO A CONMUTADOR TELEFONICO',Quantity:1,Unit:'SERVICIO'}
    ]
  },
  {
    id:'',
    Fecha: new Date().toJSON().slice(0,10),
    Status: 'Aceptado',
    Code:'123ASD',
    Justification:'SE REQUIEREN PAQUETES DE HOJAS BLANCAS PARA IMPRIMIR REPORTES Y DOCUMENTOS',
    Type:'COMPRA',
    Area:'Area 1',
    EstimatedAmount: 0.00,
    Description : [
      {Name:'PAQUETE DE HOJAS BLANCAS',Quantity:1,Unit:'PAQUETE'},
      {Name:'IMPRESORA HP',Quantity:1,Unit:'PIEZA'}
    ]
  },
  {
    id:'',
    Fecha: new Date().toJSON().slice(0,10),
    Status: 'en proceso',
    Code:'123ASD',
    Justification:'SE REQUIEREN LAPICES Y PLUMAS PARA EL LLENADO DE FORMULARIOS Y ENCUESTAS',
    Type:'COMPRA',
    Area:'Area 2',
    EstimatedAmount: 0.00,
    Description : [
      {Name:'PAQUETE DE PLUMAS',Quantity:1,Unit:'PAQUETE'},
      {Name:'PAQUETE DE LAPICES',Quantity:1,Unit:'PAQUETE'}
    ]
  },
  {
    id:'',
    Fecha: new Date().toJSON().slice(0,10),
    Status: 'en proceso',
    Code:'123ASD',
    Justification:'SE REQUIEREN TELEVISIONES PARA EL AREA DE CAPACITACION',
    Type:'COMPRA',
    Area:'Area 3',
    EstimatedAmount: 0.00,
    Description : [
      {Name:'TELEVISION',Quantity:3,Unit:'PIEZA'},
      
    ]
  },
  {
    id:'',
    Fecha: new Date().toJSON().slice(0,10),
    Status: 'Negado',
    Code:'123ASD',
    Justification:'SE REQUIERE PINTURA PARA FAMILIAS DE ESCASOS RECURSOS',
    Type:'COMPRA',
    Area:'Area 4',
    EstimatedAmount: 0.00,
    Description : [
      {Name:'BOTES DE PINTURA',Quantity:8,Unit:'BOTE'},
    ]
  }   
];

export const Response:Log ={
  Accepted: false,
  Message:'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTUzNDI5NTU3MywiZXhwIjoxNTM0Mjk5MTczLCJuYmYiOjE1MzQyOTU1NzMsImp0aSI6ImpuZmJsODZBZ3FVbUtRc0IiLCJzdWIiOjMyLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.uxtufBXo5ZWzblQLvSvf81ZNEIg5mvsjF2A146JoIwI'
};
import { Book } from "../models/book";
import { Auth } from "../models/auth";


export const books: Book[] = [
    {name: 'Danza',autor:'Beto',nPaginas:100},
    {name: 'Musica',autor:'Alex',nPaginas:50},
    {name: 'Teatro',autor:'Omar',nPaginas:70}
];

//export const auth: Auth = {
//    success: true,
//    message:'qwerdsf23'
//}

//export const auth: Auth = {
//    success: false,
//    message:'datos incorrectos'
//}
//
export const auth: Auth = {
    success: false,
    message:'usuario bloqueado'
}
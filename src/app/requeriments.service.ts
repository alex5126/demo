import { Injectable } from '@angular/core';
import { Requeriments} from './mocks/mock-requerimets'
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap} from 'rxjs/operators';
import { Format,RequerimentFormat, Requeriment } from './models/format';

const httpOptions = {
  headers: new HttpHeaders({ 'Authorization': 'application/json', 'Accept': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTUzNDI5NTU3MywiZXhwIjoxNTM0Mjk5MTczLCJuYmYiOjE1MzQyOTU1NzMsImp0aSI6ImpuZmJsODZBZ3FVbUtRc0IiLCJzdWIiOjMyLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.uxtufBXo5ZWzblQLvSvf81ZNEIg5mvsjF2A146JoIwI' })
};

@Injectable({
  providedIn: 'root'
})
export class RequerimentService {

  private requerimentsUrl = 'http://demobackend.mutua.tech/api/requeriments';

  tem: RequerimentFormat[];
  temp: any;
  constructor(private http: HttpClient) { }

  //getRequeriments(): Observable<Format> {
  //  return this.http.get<Format>(this.requerimentsUrl)
  //  .pipe(catchError(error => {
  //      console.log('Error Occurred');
  //      console.log(error);
  //      return Observable.throw(error);
  //    })) as any;
  //}

  getRequeriments(): Observable<RequerimentFormat[]>{
    return of(Requeriments);
  }

  addRequeriment(requeriment: RequerimentFormat):Observable<RequerimentFormat>{
    return this.http.post<RequerimentFormat>(this.requerimentsUrl, requeriment).pipe(catchError(error => {
      console.log('Error Occurred');
      console.log(error);
      return Observable.throw(error);
    })) as any;
  }

  updateHero (requeriment: RequerimentFormat): Observable<any> {
    return this.http.put('http://demobackend.mutua.tech/api/requeriments/'+ requeriment.id, requeriment, httpOptions).pipe(
      catchError(error => {
      console.log('Error Occurred');
      console.log(error);
      return Observable.throw(error);
    })) as any;
  }
  
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';

import { AppComponent } from './app.component';
import { RequerimentsComponent } from './components/requeriments/requeriments.component';
import { FinancesComponent } from './components/finances/finances.component';
import { ConsultationComponent } from './components/consultation/consultation.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { RequerimentService } from './requeriments.service';
import { TestComponent } from './components/test/test.component';
import { LogComponent } from './components/log/log.component';


@NgModule({
  declarations: [
    AppComponent,
    RequerimentsComponent,
    FinancesComponent,
    ConsultationComponent,
    TestComponent,
    LogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    RequerimentService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

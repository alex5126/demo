import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RequerimentsComponent } from './components/requeriments/requeriments.component';
import { FinancesComponent } from './components/finances/finances.component';
import { ConsultationComponent } from './components/consultation/consultation.component';
import { TestComponent } from './components/test/test.component';
import { LogComponent } from './components/log/log.component';

const routes: Routes = [
  { path: 'requeriments', component: RequerimentsComponent },
  { path: 'finances', component: FinancesComponent },
  { path: 'consult', component: ConsultationComponent },
  { path: 'test', component: TestComponent },
  { path: 'login', component: LogComponent }
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

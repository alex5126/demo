import { TestBed, inject } from '@angular/core/testing';

import { RequerimentService } from './requeriments.service';

describe('RequerimentsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RequerimentService]
    });
  });

  it('should be created', inject([RequerimentService], (service: RequerimentService) => {
    expect(service).toBeTruthy();
  }));
});

import { Component, OnInit } from '@angular/core'; 
import { RequerimentFormat, Requeriment } from '../../models/requerimentsFormat';
import { RequerimentService } from '../../requeriments.service';

@Component({
  selector: 'app-requeriments',
  templateUrl: './requeriments.component.html',
  styleUrls: ['./requeriments.component.css']
})
export class RequerimentsComponent implements OnInit {

  public requeriments: Requeriment [];

  public form: RequerimentFormat;
  constructor(private requrimentService: RequerimentService) { }

  ngOnInit() {
    this.clear();
  }

  send(){
    this.requrimentService.addRequeriment(this.form)
    .subscribe(form => this.form = form);
    alert("Formulario enviado");
    this.clear();
  }

  clear(){
    this.requeriments =[
      {Name:'',Quantity:0,Unit:''},
      {Name:'',Quantity:0,Unit:''},
      {Name:'',Quantity:0,Unit:''},
      {Name:'',Quantity:0,Unit:''},
      {Name:'',Quantity:0,Unit:''}
    ];

    this.form = {
      id:'',
      Fecha: new Date().toJSON().slice(0,10),
      Status: 'en proceso',
      Code:'',
      Justification:'',
      Type:'Compra',
      Area:'',
      EstimatedAmount: 0.00,
      Description : this.requeriments
    };
  }
}

import { Component, OnInit } from '@angular/core';
import { BookService } from '../../book.service';
import { Book } from '../../models/book';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor(private bookService: BookService) { }

  public booksF: Book[];
  public booksS: Book[];

  public test: Book = {
    name:'',
    autor:'',
    nPaginas:0
  };

  ngOnInit() {
    this.booksF = this.bookService.getBooksFirst();
    this.getBooks();
  }

  getBooks():void{
    this.bookService.getBooks()
    .subscribe(book => this.booksS = book);
  }

  
  //f(x) = x + 2
  //f(3) = 3 + 2

  //x => x + 2
  //3 => 3 + 2

}

import { Component, OnInit } from '@angular/core';
import { Requeriment, RequerimentFormat } from '../../models/requerimentsFormat';
import { RequerimentService } from '../../requeriments.service';
import { Format } from '../../models/format';
import { delay } from 'q';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-finances',
  templateUrl: './finances.component.html',
  styleUrls: ['./finances.component.css']
})
export class FinancesComponent implements OnInit {

  public forms: RequerimentFormat[];

  constructor(private requrimentService: RequerimentService, private http:HttpClient) { }

  ngOnInit() {
    this.getRequeriments();   
  }

  getRequeriments(): void{
    this.requrimentService.getRequeriments()
    .subscribe(response => this.forms = response);
  }

  updateRequeriment(form:RequerimentFormat, status: string): void{
    console.log(form);
    console.log(status);
    form.Status = status;
    this.requrimentService.updateHero(form)
    .subscribe(form => form = form);
  }

}

import { Component, OnInit } from '@angular/core';
import { BookService } from '../../book.service';
import { Login, Auth } from '../../models/auth';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css']
})
export class LogComponent implements OnInit {

  constructor(private service: BookService) { }

  log: Login;
  auth: Auth;

  ngOnInit() {
    this.log = {
      email:'',
      password:''
    }
  }

  sendData(){
    console.log(this.log);
    this.service.getAuth(this.log)
    .subscribe(auth => this.auth = auth);
    this.verificate();
  }

  verificate(){
    if(this.auth.success){
      alert("usuario valido");
      


    }else{
      alert(this.auth.message);
    }
  }

}
